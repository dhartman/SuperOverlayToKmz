#! python3

import threading
import time


class InvalidBarType(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class LoadingBar(threading.Thread):

    def __init__(self, list_to_be_filled, num_to_fill, bar_type=1):
        super().__init__()
        self.loading_symbols = ['\\', '|', '/', '-']
        self.list_object = list_to_be_filled
        self.num_to_fill = num_to_fill
        self.exit = False
        if bar_type == 1:
            self.loading_string = '\r[{beg}{item}{end}] '
        elif bar_type == 2:
            self.loading_string = '\r[{beg}{item}{end}] {low} '
        elif bar_type == 3:
            self.loading_string = '\r[{beg}{item}{end}] {low}/{high} '
        elif bar_type == 4:
            self.loading_string = '\r[{beg}{item}{end}] {percent}% '
        elif bar_type == 5:
            self.loading_string = '\r[{beg}{item}{end}] {low}/{high} {percent}% '
        else:
            raise InvalidBarType(value='Select a bar_type of 1 through 5')

        if not isinstance(num_to_fill, int) or num_to_fill < 1:
            raise InvalidBarType('num_to_fill must be an integer and ')

    def run(self):
        if self.num_to_fill > 20:
            scale = 20 / self.num_to_fill
        else:
            scale = 1

        loop = True
        while loop and not self.exit:
            for item in self.loading_symbols:
                i = int(len(self.list_object) * scale)
                if i == int(self.num_to_fill * scale):
                    i -= 1
                    loop = False
                elif i > int(self.num_to_fill * scale):
                    print('List object\'s length exceeded number to fill.. Breaking loading bar.')
                    loop = False
                    break
                beg = i * '-'
                end = -(i-(int(self.num_to_fill * scale)-1)) * '-'
                low, high = len(self.list_object), self.num_to_fill
                print(self.loading_string.format(beg=beg, item=item, end=end, low=low, high=high,
                                                 percent=int(low*100/high)), end='')
                time.sleep(0.25)
        else:
            low, high = len(self.list_object), self.num_to_fill
            print(self.loading_string.format(beg=beg, item=item, end=end, low=low, high=high,
                                             percent=int(low*100/high)), end='')


if __name__ == '__main__':
    import time

    finished_items_list = list()
    num = 173

    myLoadingBar = LoadingBar(list_to_be_filled=finished_items_list, num_to_fill=num, bar_type=5)
    myLoadingBar.daemon = True
    myLoadingBar.start()

    for i in range(num):
        time.sleep(0.1)
        finished_items_list.append(None)

    # myLoadingBar.exit = True
    myLoadingBar.join()

