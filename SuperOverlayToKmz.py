#! python3

import os
import sys
import zipfile
import shutil
import xml.etree.ElementTree as ET
from loadingBar import LoadingBar


def unzip(file, directory):
    zip_ref = zipfile.ZipFile(file)
    zip_ref.extractall(directory)
    zip_ref.close()


def main(input_dir, display_loading_bar=True, *, delete_temp_tiles=True):
    input_directory = os.path.abspath(input_dir)
    output_directory = os.path.join(os.path.split(input_directory)[0], 'tmp_super_overlay_dir')
    tile_directory = os.path.join(output_directory, 'tiles')

    if os.path.isdir(output_directory):
        print(output_directory, 'already exits. Exiting')
        sys.exit(1)
    else:
        os.mkdir(output_directory)
        os.mkdir(tile_directory)

    finished_list = list()
    print('Making temporary reformatted tiles...')
    if display_loading_bar:
        myLoadingBar = LoadingBar(list_to_be_filled=finished_list, num_to_fill=len(os.listdir(input_directory)),
                                  bar_type=5)
        myLoadingBar.daemon = True
        myLoadingBar.start()

    for name in os.listdir(input_directory):
        if name.endswith('.kml'):
            doc_file = os.path.join(output_directory, 'doc.kml')
            shutil.copyfile(os.path.join(input_directory, name), doc_file)
            kmz_name = os.path.splitext(name)[0]
            if kmz_name.endswith('_root'):
                kmz_name = kmz_name[:-5]
            kmz_name += '.kmz'
        elif name.endswith('.kmz'):
            file = os.path.join(input_directory, name)
            unzip(file, tile_directory)
            newname = os.path.join(tile_directory, os.path.splitext(name)[0] + '.kml')
            os.rename(os.path.join(tile_directory, 'doc.kml'), newname)
            with open(newname, 'r') as fn:
                text = fn.read()
            text = text.replace('.kmz', '.kml')
            with open(newname, 'w') as fn:
                fn.write(text)
        else:
            print('\nFound unexpected name:', name, '\nExiting.')
            sys.exit(1)
        finished_list.append(name)
    else:
        lat, lon = get_average_coords(tile_directory)
        distance = get_range(tile_directory)
        with open(doc_file, 'r') as fn:
            text = fn.read()
        text = text.replace('.kmz</href>', '.kml</href>', 1)\
            .replace('>\n<NetworkLink>', '>\n<Document>\n<NetworkLink>', 1)\
            .replace('</NetworkLink>\n</kml>', '</NetworkLink>\n</Document>\n</kml>', 1)\
            .replace('<Link>\n\t\t<href>', '<Link>\n\t\t<href>{}/'.format(os.path.split(tile_directory)[-1], 1))\
            .replace(' Root</name>', '</name>', 1)\
            .replace('</name>\n\t<Style', (
            '</name>'
            '\n\t<LookAt>'
            '\n\t\t<longitude>{lon}</longitude>'
            '\n\t\t<latitude>{lat}</latitude>'
            '\n\t\t<altitude>0</altitude>'
            '\n\t\t<heading>0</heading>'
            '\n\t\t<tilt>45</tilt>'
            '\n\t\t<range>{distance}</range>'
            '\n\t\t<gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>'
            '\n\t</LookAt>'
            '\n\t<Style'
            ).format(lat=lat, lon=lon, distance=distance), 1)
        with open(doc_file, 'w') as fn:
            fn.write(text)

    if display_loading_bar:
        myLoadingBar.exit = True
        myLoadingBar.join()

    # output_file = os.path.join(os.path.split(input_directory)[0], kmz_name)
    output_file = os.path.join(input_directory, kmz_name)
    if os.path.isfile(output_file):
        print(output_file, 'already exists. Exiting.')
        sys.exit(1)

    for item in os.listdir(input_directory):
        item = os.path.join(input_directory, item)
        if os.path.isfile(item):
            os.remove(item)
        elif os.path.isdir(item):
            shutil.rmtree(item)
        else:
            print('Error removing:', item)

    print('\nZipping Files into kmz...')
    zip(output_directory, output_file)

    if delete_temp_tiles:
        print('\nDeleting temporary reformatted tiles...')
        shutil.rmtree(output_directory)

    input('\nFinished. Kmz is here: {}\nPress <enter> to continue.'.format(output_file))


def get_range(tile_directory):
    files = [file for file in os.listdir(tile_directory) if os.path.isfile(os.path.join(tile_directory, file))]
    files = sorted(files)
    file_groups = dict()
    for file in files:
        key = file.split('_')[1]
        if not key in file_groups.keys():
            file_groups[key] = list()
        file_groups[key].append(file)
    distance = 475*len(file_groups)-3200
    if distance < 500:
        distance = 500
    return distance


def get_average_coords(tile_directory, file_group_index=6):
    files = [file for file in os.listdir(tile_directory) if os.path.isfile(os.path.join(tile_directory, file))]
    files = sorted(files)
    file_groups = dict()
    for file in files:
        key = file.split('_')[1]
        if not key in file_groups.keys():
            file_groups[key] = list()
        file_groups[key].append(file)
    if file_group_index >= len(file_groups):
        file_group_index = -1

    index_group = file_groups[sorted(file_groups)[file_group_index]]

    lats, lons = list(), list()

    for file in index_group:
        file = os.path.join(tile_directory, file)
        with open(file, 'r') as fn:
            tree = ET.parse(file)
            root = tree.getroot()

            ns = {'kml': 'http://www.opengis.net/kml/2.2'}
            LatLonBox = dict()
            for item in root.find('.//kml:GroundOverlay', ns).find('kml:LatLonBox', ns).getchildren():
                LatLonBox[item.tag.replace('{{{}}}'.format(ns['kml']) ,'')] = item.text

            lats += LatLonBox['north'], LatLonBox['south']
            lons += LatLonBox['east'], LatLonBox['west']

    lats = [float(item) for item in lats]
    lons = [float(item) for item in lons]

    return sum(lats)/len(lats), sum(lons)/len(lons)


def zip(src, dst, display_loading_bar=True):
    src, dst = os.path.abspath(src), os.path.abspath(dst)
    if os.path.isfile(dst):
        print(dst, 'already exists. Exiting.')
        sys.exit(1)
    elif not (dst.endswith('.kmz') or dst.endswith('.zip')):
        dst += '.kmz'

    finished_list = list()
    if display_loading_bar:
        num_of_files = 0
        for _, _, files in os.walk(src):
            num_of_files += len(files)
        myLoadingBar = LoadingBar(finished_list, num_of_files, bar_type=5)
        myLoadingBar.daemon = True
        myLoadingBar.start()

    zf = zipfile.ZipFile(dst, "w", zipfile.ZIP_DEFLATED)
    abs_src = os.path.abspath(src)
    for dirname, subdirs, files in os.walk(src):
        for filename in files:
            absname = os.path.abspath(os.path.join(dirname, filename))
            if absname == dst:
                print('Skipping', filename)
            else:
                arcname = absname[len(abs_src) + 1:]
                zf.write(absname, arcname)
            finished_list.append(filename)
    zf.close()
    if display_loading_bar:
        myLoadingBar.exit = True
        myLoadingBar.join()


def GUI():
    from tkinter import Tk, filedialog

    hide_window = Tk()
    hide_window.attributes('-alpha', 0)
    input_directory = filedialog.askdirectory()
    hide_window.destroy()

    if os.path.isdir(input_directory):
        main(input_directory)

if __name__ == '__main__':
    GUI()

    # input_directory = r'C:\PycharmProjects\SuperOverlayToKmz\Archive\Super Overlay'
    # main(input_directory)








